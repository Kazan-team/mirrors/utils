import unittest
from budget_sync.money import Money, CENTS_PER_EURO
import operator


class TestMoney(unittest.TestCase):
    def test_str(self):
        self.assertEqual("123.45", str(Money(cents=12345)))
        self.assertEqual("123", str(Money(cents=12300)))
        self.assertEqual("123.40", str(Money(cents=12340)))
        self.assertEqual("120", str(Money(cents=12000)))
        self.assertEqual("-123.45", str(Money(cents=-12345)))
        self.assertEqual("-123", str(Money(cents=-12300)))
        self.assertEqual("-123.40", str(Money(cents=-12340)))
        self.assertEqual("-120", str(Money(cents=-12000)))
        self.assertEqual("0", str(Money(cents=0)))

    def test_from_str(self):
        with self.assertRaisesRegex(TypeError, "^Can't use Money\\.from_str to convert from non-str value$"):
            Money.from_str(123)
        with self.assertRaisesRegex(ValueError, "^invalid Money string: missing digits$"):
            Money.from_str("")
        with self.assertRaisesRegex(ValueError, "^invalid Money string: missing digits$"):
            Money.from_str(".")
        with self.assertRaisesRegex(ValueError, "^invalid Money string: missing digits$"):
            Money.from_str("-")
        with self.assertRaisesRegex(ValueError, "^invalid Money string: missing digits$"):
            Money.from_str("-.")
        with self.assertRaisesRegex(ValueError, "^invalid Money string: characters after sign and before first `\\.` must be ascii digits$"):
            Money.from_str("+asdjkhfk")
        with self.assertRaisesRegex(ValueError, "^invalid Money string: too many digits after `\\.`$"):
            Money.from_str("12.345")
        with self.assertRaisesRegex(ValueError, "^invalid Money string: too many `\\.` characters$"):
            Money.from_str("12.3.4")
        self.assertEqual(Money(cents=0), Money.from_str("0"))
        self.assertEqual(Money(cents=0), Money.from_str("+0"))
        self.assertEqual(Money(cents=0), Money.from_str("-0"))
        self.assertEqual(Money(cents=-1000), Money.from_str("-010"))
        self.assertEqual(Money(cents=1000), Money.from_str("+010"))
        self.assertEqual(Money(cents=1000), Money.from_str("010"))
        self.assertEqual(Money(cents=1000), Money.from_str("10"))
        self.assertEqual(Money(cents=1234), Money.from_str("12.34"))
        self.assertEqual(Money(cents=-1234), Money.from_str("-12.34"))
        self.assertEqual(Money(cents=-1234), Money.from_str("-12.34"))
        self.assertEqual(Money(cents=1230), Money.from_str("12.3"))
        self.assertEqual(Money(cents=1200), Money.from_str("12."))
        self.assertEqual(Money(cents=1200), Money.from_str("12"))
        self.assertEqual(Money(cents=0), Money.from_str(".0"))
        self.assertEqual(Money(cents=10), Money.from_str(".1"))
        self.assertEqual(Money(cents=12), Money.from_str(".12"))
        self.assertEqual(Money(cents=-12), Money.from_str("-.12"))

    def test_repr(self):
        self.assertEqual(repr(Money("123.45")), "Money('123.45')")

    def test_cmp(self):
        for l in (-10, 10):
            for r in (-10, 10):
                self.assertEqual(l == r, Money(cents=l) == Money(cents=r))
                self.assertEqual(l != r, Money(cents=l) != Money(cents=r))
                self.assertEqual(l <= r, Money(cents=l) <= Money(cents=r))
                self.assertEqual(l >= r, Money(cents=l) >= Money(cents=r))
                self.assertEqual(l < r, Money(cents=l) < Money(cents=r))
                self.assertEqual(l > r, Money(cents=l) > Money(cents=r))

    def test_bool(self):
        for i in range(-10, 10):
            self.assertEqual(bool(Money(cents=i)), bool(i))

    def add_sub_helper(self, op):
        for l in range(-10, 10):
            for r in range(-10, 10):
                self.assertEqual(op(l, r * CENTS_PER_EURO),
                                 op(Money(cents=l), r).cents)
                self.assertEqual(op(l * CENTS_PER_EURO, r),
                                 op(l, Money(cents=r)).cents)
                self.assertEqual(op(l, r),
                                 op(Money(cents=l), Money(cents=r)).cents)

    def test_add(self):
        self.add_sub_helper(operator.add)
        self.add_sub_helper(operator.iadd)

    def test_sub(self):
        self.add_sub_helper(operator.sub)
        self.add_sub_helper(operator.isub)

    def mul_helper(self, op):
        for l in range(-10, 10):
            for r in range(-10, 10):
                self.assertEqual(op(l, r),
                                 op(Money(cents=l), r).cents)
                self.assertEqual(op(l, r),
                                 op(l, Money(cents=r)).cents)
                with self.assertRaises(TypeError):
                    op(Money(cents=l), Money(cents=r))

    def test_mul(self):
        self.mul_helper(operator.mul)
        self.mul_helper(operator.imul)


if __name__ == "__main__":
    unittest.main()
