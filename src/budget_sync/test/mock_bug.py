from typing import Optional, Union
from budget_sync.util import BugStatus


class MockBug:
    def __init__(self,
                 bug_id: int,
                 cf_budget_parent: Optional[int] = None,
                 cf_budget: str = "0",
                 cf_total_budget: str = "0",
                 cf_nlnet_milestone: Optional[str] = None,
                 cf_payees_list: str = "",
                 summary: str = "<default summary>",
                 status: Union[str, BugStatus] = BugStatus.CONFIRMED,
                 assigned_to: str = "user@example.com"):
        self.id = bug_id
        self.__budget_parent = cf_budget_parent
        self.cf_budget = cf_budget
        self.cf_total_budget = cf_total_budget
        if cf_nlnet_milestone is None:
            cf_nlnet_milestone = "---"
        self.cf_nlnet_milestone = cf_nlnet_milestone
        self.cf_payees_list = cf_payees_list
        self.summary = summary
        self.status = str(status)
        self.assigned_to = assigned_to

    @property
    def cf_budget_parent(self) -> int:
        if self.__budget_parent is None:
            raise AttributeError(
                "'MockBug' object has no attribute 'cf_budget_parent'")
        return self.__budget_parent

    @cf_budget_parent.setter
    def cf_budget_parent(self, value: int):
        if isinstance(value, int):
            self.__budget_parent = value
        else:
            raise TypeError("cf_budget_parent must be an int")

    @cf_budget_parent.deleter
    def cf_budget_parent(self):
        self.cf_budget_parent  # trigger AttributeError if property cleared
        self.__budget_parent = None

    def __repr__(self):
        cf_budget_parent = getattr(self, "cf_budget_parent", None)
        status = BugStatus.cast(self.status, unknown_allowed=True)
        return (f"MockBug(bug_id={self.id!r}, "
                f"cf_budget_parent={cf_budget_parent!r}, "
                f"cf_budget={self.cf_budget!r}, "
                f"cf_total_budget={self.cf_total_budget!r}, "
                f"cf_nlnet_milestone={self.cf_nlnet_milestone!r}, "
                f"cf_payees_list={self.cf_payees_list!r}, "
                f"summary={self.summary!r}, "
                f"status={status!r}, "
                f"assigned_to={self.assigned_to!r})")
