import unittest
from budget_sync.ordered_set import OrderedSet


class TestOrderedSet(unittest.TestCase):
    def test_repr(self):
        self.assertEqual(repr(OrderedSet()), "OrderedSet()")
        self.assertEqual(repr(OrderedSet((1,))), "OrderedSet([1])")
        self.assertEqual(repr(OrderedSet((1, 2))), "OrderedSet([1, 2])")
        self.assertEqual(repr(OrderedSet((2, 1))), "OrderedSet([2, 1])")
        self.assertEqual(repr(OrderedSet((2, 2))), "OrderedSet([2])")

    def test_len(self):
        self.assertEqual(len(OrderedSet()), 0)
        self.assertEqual(len(OrderedSet((1,))), 1)
        self.assertEqual(len(OrderedSet((1, 2))), 2)
        self.assertEqual(len(OrderedSet((2, 1))), 2)
        self.assertEqual(len(OrderedSet((2, 2))), 1)

    def test_contains(self):
        self.assertFalse(0 in OrderedSet())
        self.assertFalse(1 in OrderedSet())
        self.assertTrue(0 in OrderedSet([0]))
        self.assertFalse(1 in OrderedSet([0]))
        self.assertTrue(0 in OrderedSet([0, 1]))
        self.assertTrue(1 in OrderedSet([0, 1]))
        self.assertTrue(0 in OrderedSet([1, 0]))
        self.assertTrue(1 in OrderedSet([1, 0]))

    def test_add(self):
        s = OrderedSet()
        self.assertEqual(repr(s), "OrderedSet()")
        s.add(1)
        self.assertEqual(repr(s), "OrderedSet([1])")
        s.add(2)
        self.assertEqual(repr(s), "OrderedSet([1, 2])")
        s.add(2)
        self.assertEqual(repr(s), "OrderedSet([1, 2])")
        s.add(1)
        self.assertEqual(repr(s), "OrderedSet([1, 2])")
        s.add(0)
        self.assertEqual(repr(s), "OrderedSet([1, 2, 0])")

    def test_discard(self):
        s = OrderedSet()
        s.discard(1)
        self.assertEqual(repr(s), "OrderedSet()")
        s = OrderedSet([1])
        s.discard(1)
        self.assertEqual(repr(s), "OrderedSet()")
        s = OrderedSet([1, 2, 3])
        s.discard(1)
        self.assertEqual(repr(s), "OrderedSet([2, 3])")
        s = OrderedSet([3, 2, 1])
        s.discard(1)
        self.assertEqual(repr(s), "OrderedSet([3, 2])")
        s = OrderedSet([3, 2, 1])
        s.discard(None)
        self.assertEqual(repr(s), "OrderedSet([3, 2, 1])")

    def test_iter(self):
        self.assertEqual(list(OrderedSet()), [])
        self.assertEqual(list(OrderedSet((1,))), [1])
        self.assertEqual(list(OrderedSet((1, 2))), [1, 2])
        self.assertEqual(list(OrderedSet((2, 1))), [2, 1])
        self.assertEqual(list(OrderedSet((2, 2))), [2])


if __name__ == "__main__":
    unittest.main()
