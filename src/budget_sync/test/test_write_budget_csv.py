from budget_sync.budget_graph import BudgetGraph
from budget_sync.config import Config
from budget_sync.test.mock_path import DIR, MockPath
from budget_sync.test.test_mock_path import make_filesystem_and_report_if_error
from budget_sync.util import pretty_print
from budget_sync.write_budget_csv import write_budget_csv
from budget_sync.test.mock_bug import MockBug
import unittest


class TestWriteBudgetMarkdown(unittest.TestCase):
    maxDiff = None

    def test(self):
        config = Config.from_str(
            """
            bugzilla_url = "https://bugzilla.example.com/"
            [people."person1"]
            aliases = ["person1_alias1", "alias1"]
            full_name = "Person One"
            [people."person2"]
            email = "person2@example.com"
            aliases = ["person1_alias2", "alias2", "person 2"]
            full_name = "Person Two"
            [people."person3"]
            email = "user@example.com"
            full_name = "Person Three"
            [milestones]
            "milestone 1" = { canonical_bug_id = 1 }
            "milestone 2" = { canonical_bug_id = 2 }
            """)
        budget_graph = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="1000",
                    cf_total_budget="1000",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="""
                    person1 = 123
                    alias1 = 456
                    person2 = {amount=421,paid=2020-01-01}
                    """,
                    summary="",
                    assigned_to="person2@example.com"),
            MockBug(bug_id=2,
                    cf_budget_parent=None,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone="milestone 2",
                    cf_payees_list="",
                    summary="",
                    assigned_to="person2@example.com"),
        ], config)
        self.assertEqual([], budget_graph.get_errors())
        # pretty_print(budget_graph)
        with make_filesystem_and_report_if_error(self) as filesystem:
            output_dir = MockPath("/output_dir/", filesystem=filesystem)
            write_budget_csv(budget_graph, output_dir)
            self.assertEqual(filesystem.files, {
                '/': DIR,
                '/output_dir': DIR,
                '/output_dir/csvs.mdwn': b"""\
# milestone 1

[[!table format=csv file="/output_dir/milestone 1.csv"]]
# milestone 2

[[!table format=csv file="/output_dir/milestone 2.csv"]]""",
                '/output_dir/milestone 1.csv': b"""\
bug_id,excl_subtasks,inc_subtasks,fixed_excl_subtasks,fixed_inc_subtasks,req_excl_subtasks,paid_excl_subtasks,person1 (planned amt),person1 (req amt),person1 (req date),person1 (paid amt),person1 (paid date),person2 (planned amt),person2 (req amt),person2 (req date),person2 (paid amt),person2 (paid date)
1,1000,1000,1000,1000,421,421,579,0,,0,,421,421,,421,2020-01-01
""",
                '/output_dir/milestone 2.csv': b"""\
bug_id,excl_subtasks,inc_subtasks,fixed_excl_subtasks,fixed_inc_subtasks,req_excl_subtasks,paid_excl_subtasks
"""
            })
    # TODO: add more test cases


if __name__ == "__main__":
    unittest.main()
