import unittest
import io
from budget_sync.config import Config, ConfigParseError


class TestConfig(unittest.TestCase):
    maxDiff = None

    def test_config_parsing(self):
        def check_error(text: str, expected_error_text: str):
            with self.assertRaises(ConfigParseError) as e:
                Config.from_str(text)
            self.assertEqual(str(e.exception), expected_error_text)

        def check(text: str, expected_repr_text: str):
            self.assertEqual(repr(Config.from_str(text)), expected_repr_text)

        check_error(
            "bad-toml=",
            "TOML parse error: Empty value is invalid "
            "(line 1 column 1 char 0)")
        check_error(
            """
            """,
            "`bugzilla_url` field is missing")
        check_error(
            """
            [bugzilla_url]
            """,
            "`bugzilla_url` must be a string")
        check_error(
            """
            bugzilla_url = ""
            """,
            "`people` table is missing")
        check_error(
            """
            blah = ""
            """,
            "unknown config entry: `blah`")
        check_error(
            """
            bugzilla_url = ""
            people = []
            """,
            "`people` field must be a table")
        check_error(
            """
            bugzilla_url = ""
            [people]
            person1 = 1
            """,
            "person entry for 'person1' must be a table")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            aliases = ""
            """,
            "`aliases` field in person entry for 'person1' must be a list "
            "of strings")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            aliases = [1]
            """,
            "`aliases` field in person entry for 'person1' must be a list "
            "of strings")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            aliases = ["a", "a"]
            """,
            "duplicate alias in person entry for 'person1': 'a'")
        check_error(
            """
            bugzilla_url = ""
            [people]
            """,
            "`milestones` table is missing")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            """,
            "`full_name` field is missing in person entry for "
            "'person1'")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            full_name = 1
            """,
            "`full_name` field in person entry for 'person1' must "
            "be a string")
        check(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            aliases = ["a"]
            full_name = "Person One"
            [people."person2"]
            aliases = ["b"]
            full_name = "Person Two"
            """,
            "Config(bugzilla_url='', people={"
            "'person1': Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['a']), email=None), "
            "'person2': Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['b']), email=None)}, milestones={})")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            email = 123
            full_name = "Person One"
            """,
            "`email` field in person entry for 'person1' must be a string")
        check(
            """
            bugzilla_url = ""
            [people]
            [milestones]
            """,
            "Config(bugzilla_url='', people={}, milestones={})")
        check(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            email = "email@example.com"
            full_name = "Person One"
            """,
            "Config(bugzilla_url='', people={"
            "'person1': Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(), email='email@example.com')}, "
            "milestones={})")
        check_error(
            """
            bugzilla_url = ""
            [people."person1"]
            blah = 123
            full_name = "Person One"
            """,
            "unknown field in person entry for 'person1': `blah`")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            full_name = "Person One"
            [people."person2"]
            aliases = ["person1"]
            full_name = "Person Two"
            """,
            "alias is not allowed to be the same as any person's identifier: "
            "in person entry for 'person2': 'person1' is also the identifier "
            "for person 'person1'")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            full_name = "Person One"
            aliases = ["a"]
            [people."person2"]
            aliases = ["a"]
            full_name = "Person Two"
            """,
            "alias is not allowed to be the same as another person's alias, "
            "email, or full_name: in person entry for 'person2': 'a' is also an alias, "
            "email, or full_name for person 'person1'")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            full_name = "Person One"
            aliases = ["abc@example.com"]
            [people."person2"]
            email = "abc@example.com"
            full_name = "Person Two"
            """,
            "email is not allowed to be the same as another person's alias, "
            "email, or full_name: in person entry for 'person2': 'abc@example.com' is also "
            "an alias, email, or full_name for person 'person1'")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            full_name = "Person One"
            aliases = ["Person Two"]
            [people."person2"]
            email = "abc@example.com"
            full_name = "Person Two"
            """,
            "full_name is not allowed to be the same as another person's alias, "
            "email, or full_name: in person entry for 'person2': 'Person Two' is also "
            "an alias, email, or full_name for person 'person1'")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            [people."person2"]
            email = "abc@example.com"
            full_name = "Person Two"
            [people."person1"]
            full_name = "Person One"
            aliases = ["abc@example.com"]
            """,
            "alias is not allowed to be the same as another person's alias, "
            "email, or full_name: in person entry for 'person1': 'abc@example.com' is also "
            "an alias, email, or full_name for person 'person2'")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            "abc" = 1
            [people]
            """,
            "milestones entry for 'abc' must be a table")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            "abc" = { canonical_bug_id = "abc" }
            [people]
            """,
            "`canonical_bug_id` field in milestones entry for 'abc' must "
            "be an integer")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            "abc" = { blah = "def" }
            [people]
            """,
            "unknown field in milestones entry for 'abc': `blah`")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            "abc" = {}
            [people]
            """,
            "`canonical_bug_id` field is missing in milestones entry for 'abc'")
        check_error(
            """
            bugzilla_url = ""
            milestones = 1
            [people]
            """,
            "`milestones` field must be a table")
        check_error(
            """
            bugzilla_url = ""
            [milestones]
            "abc" = { canonical_bug_id = 1 }
            "def" = { canonical_bug_id = 1 }
            [people]
            """,
            "canonical_bug_id is not allowed to be the same as another "
            "milestone's canonical_bug_id: in milestone entry for 'def': "
            "1 is also the canonical_bug_id for milestone 'abc'")

    def test_all_names(self):
        config = Config.from_str(
            """
            bugzilla_url = ""
            [milestones]
            [people."person1"]
            aliases = ["person1_alias1", "alias1"]
            full_name = "Person One"
            [people."person2"]
            aliases = ["person2_alias2", "alias2"]
            full_name = "Person Two"
            """)
        person1 = config.people['person1']
        person2 = config.people['person2']
        self.assertEqual(config.all_names,
                         {
                             'person1': person1,
                             'Person One': person1,
                             'person1_alias1': person1,
                             'alias1': person1,
                             'person2': person2,
                             'Person Two': person2,
                             'person2_alias2': person2,
                             'alias2': person2,
                         })

    def test_canonical_bug_ids(self):
        config = Config.from_str(
            """
            bugzilla_url = ""
            [people]
            [milestones]
            "Milestone 1" = { canonical_bug_id = 1 }
            "Milestone 2" = { canonical_bug_id = 2 }
            """)
        milestone1 = config.milestones['Milestone 1']
        milestone2 = config.milestones['Milestone 2']
        self.assertEqual(config.canonical_bug_ids,
                         {
                             1: milestone1,
                             2: milestone2,
                         })

    def test_bugzilla_url_stripped(self):
        c = Config.from_str(
            """
            bugzilla_url = "https://bugzilla.example.com/prefix"
            [people]
            [milestones]
            """
        )
        self.assertEqual(c.bugzilla_url_stripped,
                         "https://bugzilla.example.com/prefix")
        c = Config.from_str(
            """
            bugzilla_url = "https://bugzilla.example.com/prefix/"
            [people]
            [milestones]
            """
        )
        self.assertEqual(c.bugzilla_url_stripped,
                         "https://bugzilla.example.com/prefix")
        c = Config.from_str(
            """
            bugzilla_url = "https://bugzilla.example.com/"
            [people]
            [milestones]
            """
        )
        self.assertEqual(c.bugzilla_url_stripped,
                         "https://bugzilla.example.com")

    def test_from_file(self):
        def load(text):
            with io.StringIO(text) as file:
                return Config.from_file(file)

        with self.assertRaisesRegex(TypeError,
                                    "^list is not a valid file or path$"):
            Config.from_file([])

        with self.assertRaisesRegex(
                ConfigParseError,
                "^TOML parse error: Empty value is invalid"):
            load("""bad-toml=""")

        self.assertEqual(str(load(
            """
            bugzilla_url = "https://bugzilla.example.com/"
            [people."person1"]
            email = "person1@example.com"
            aliases = ["alias1"]
            full_name = "Person One"
            [milestones]
            "Milestone 1" = { canonical_bug_id = 123 }
            """)),
            "Config(bugzilla_url='https://bugzilla.example.com/', "
            "people={'person1': Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['alias1']), email='person1@example.com')}, "
            "milestones={'Milestone 1': Milestone(config=..., "
            "identifier='Milestone 1', canonical_bug_id=123)})")


if __name__ == "__main__":
    unittest.main()
