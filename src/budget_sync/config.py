from budget_sync.ordered_set import OrderedSet
from budget_sync.util import PrettyPrinter
import toml
import sys
from typing import Mapping, Set, Dict, Any, Optional
try:
    from functools import cached_property
except ImportError:
    # compatability with python < 3.8
    from cached_property import cached_property


class ConfigParseError(Exception):
    pass


class Person:
    aliases: OrderedSet[str]
    email: Optional[str]

    def __init__(self, config: "Config", identifier: str,
                 full_name: str,
                 aliases: Optional[OrderedSet[str]] = None,
                 email: Optional[str] = None):
        self.config = config
        self.identifier = identifier
        if aliases is None:
            aliases = OrderedSet()
        else:
            assert isinstance(aliases, OrderedSet)
        self.aliases = aliases
        self.email = email
        self.full_name = full_name

    @cached_property
    def all_names(self) -> OrderedSet[str]:
        retval = OrderedSet(self.aliases)
        retval.add(self.identifier)
        retval.add(self.full_name)
        if self.email is not None:
            retval.add(self.email)
        return retval

    @cached_property
    def output_markdown_file(self) -> str:
        return self.identifier + '.mdwn'

    def __eq__(self, other):
        return self.identifier == other.identifier

    def __hash__(self):
        return hash(self.identifier)

    def __pretty_print__(self, pp: PrettyPrinter):
        with pp.type_pp("Person") as tpp:
            tpp.field("config", ...)
            tpp.field("identifier", self.identifier)

    def __repr__(self):
        return (f"Person(config=..., identifier={self.identifier!r}, "
                f"full_name={self.full_name!r}, "
                f"aliases={self.aliases!r}, email={self.email!r})")


class Milestone:
    def __init__(self, config: "Config",
                 identifier: str, canonical_bug_id: int):
        self.config = config
        self.identifier = identifier
        self.canonical_bug_id = canonical_bug_id

    def __eq__(self, other):
        return self.identifier == other.identifier

    def __hash__(self):
        return hash(self.identifier)

    def __repr__(self):
        return f"Milestone(config=..., " \
            f"identifier={self.identifier!r}, " \
            f"canonical_bug_id={self.canonical_bug_id})"


class Config:
    def __init__(self, bugzilla_url: str, people: Dict[str, Person],
                 milestones: Dict[str, Milestone]):
        self.bugzilla_url = bugzilla_url
        self.people = people
        self.milestones = milestones

    def __repr__(self):
        return f"Config(bugzilla_url={self.bugzilla_url!r}, " \
            f"people={self.people!r}, " \
            f"milestones={self.milestones!r})"

    @cached_property
    def bugzilla_url_stripped(self):
        return self.bugzilla_url.rstrip('/')

    @cached_property
    def all_names(self) -> Dict[str, Person]:
        # also checks for any name clashes and raises
        # ConfigParseError if any are detected
        retval = self.people.copy()

        for person in self.people.values():
            for name in person.all_names:
                other_person = retval.get(name)
                if other_person is not None and other_person is not person:
                    alias_or_email_or_full_name = "alias"
                    if name == person.email:
                        alias_or_email_or_full_name = "email"
                    if name == person.full_name:
                        alias_or_email_or_full_name = "full_name"
                    if name in self.people:
                        raise ConfigParseError(
                            f"{alias_or_email_or_full_name} is not allowed "
                            f"to be the same "
                            f"as any person's identifier: in person entry for "
                            f"{person.identifier!r}: {name!r} is also the "
                            f"identifier for person"
                            f" {other_person.identifier!r}")
                    raise ConfigParseError(
                        f"{alias_or_email_or_full_name} is not allowed "
                        f"to be the same as "
                        f"another person's alias, email, or full_name: "
                        f"in person entry "
                        f"for {person.identifier!r}: {name!r} is also an alias"
                        f", email, or full_name for person "
                        f"{other_person.identifier!r}")
                retval[name] = person
        return retval

    @cached_property
    def canonical_bug_ids(self) -> Dict[int, Milestone]:
        # also checks for any bug id clashes and raises
        # ConfigParseError if any are detected
        retval: Dict[int, Milestone] = {}
        for milestone in self.milestones.values():
            other_milestone = retval.get(milestone.canonical_bug_id)
            if other_milestone is not None:
                raise ConfigParseError(
                    f"canonical_bug_id is not allowed to be the same as "
                    f"another milestone's canonical_bug_id: in milestone "
                    f"entry for {milestone.identifier!r}: "
                    f"{milestone.canonical_bug_id} is also the "
                    f"canonical_bug_id for milestone "
                    f"{other_milestone.identifier!r}")
            retval[milestone.canonical_bug_id] = milestone
        return retval

    def _parse_person(self, identifier: str, value: Any) -> Person:
        def raise_aliases_must_be_list_of_strings():
            raise ConfigParseError(
                f"`aliases` field in person entry for {identifier!r} must "
                f"be a list of strings")

        if not isinstance(value, dict):
            raise ConfigParseError(
                f"person entry for {identifier!r} must be a table")
        aliases = OrderedSet()
        email = None
        full_name = None
        for k, v in value.items():
            assert isinstance(k, str)
            if k == "aliases":
                if not isinstance(v, list):
                    raise_aliases_must_be_list_of_strings()
                for alias in v:
                    if isinstance(alias, str):
                        if alias in aliases:
                            raise ConfigParseError(
                                f"duplicate alias in person entry for "
                                f"{identifier!r}: {alias!r}")
                        aliases.add(alias)
                    else:
                        raise_aliases_must_be_list_of_strings()
            elif k == "email":
                if not isinstance(v, str):
                    raise ConfigParseError(
                        f"`email` field in person entry for {identifier!r} "
                        f"must be a string")
                email = v
            elif k == "full_name":
                if not isinstance(v, str):
                    raise ConfigParseError(
                        f"`full_name` field in person entry for "
                        f"{identifier!r} must be a string")
                full_name = v
            else:
                raise ConfigParseError(
                    f"unknown field in person entry for {identifier!r}: `{k}`")
        if full_name is None:
            raise ConfigParseError(f"`full_name` field is missing in "
                                   f"person entry for {identifier!r}")
        return Person(config=self, identifier=identifier,
                      full_name=full_name,
                      aliases=aliases, email=email)

    def _parse_people(self, people: Any):
        if not isinstance(people, dict):
            raise ConfigParseError("`people` field must be a table")
        for identifier, value in people.items():
            assert isinstance(identifier, str)
            self.people[identifier] = self._parse_person(identifier, value)

        # self.all_names checks for name clashes and raises ConfigParseError
        # if any are detected, so the following line is needed:
        self.all_names

    def _parse_milestone(self, identifier: str, value: Any) -> Milestone:
        if not isinstance(value, dict):
            raise ConfigParseError(
                f"milestones entry for {identifier!r} must be a table")
        canonical_bug_id = None
        for k, v in value.items():
            assert isinstance(k, str)
            if k == "canonical_bug_id":
                if not isinstance(v, int):
                    raise ConfigParseError(
                        f"`canonical_bug_id` field in milestones entry for "
                        f"{identifier!r} must be an integer")
                canonical_bug_id = v
            else:
                raise ConfigParseError(f"unknown field in milestones entry "
                                       f"for {identifier!r}: `{k}`")
        if canonical_bug_id is None:
            raise ConfigParseError(f"`canonical_bug_id` field is missing in "
                                   f"milestones entry for {identifier!r}")
        return Milestone(config=self, identifier=identifier,
                         canonical_bug_id=canonical_bug_id)

    def _parse_milestones(self, milestones: Any):
        if not isinstance(milestones, dict):
            raise ConfigParseError("`milestones` field must be a table")
        for identifier, value in milestones.items():
            assert isinstance(identifier, str)
            self.milestones[identifier] = \
                self._parse_milestone(identifier, value)

        # self.canonical_bug_ids checks for bug id clashes and raises
        # ConfigParseError if any are detected, so the following line
        # is needed:
        self.canonical_bug_ids

    @staticmethod
    def _from_toml(parsed_toml: Mapping[str, Any]) -> "Config":
        people = None
        bugzilla_url = None
        milestones = None
        for k, v in parsed_toml.items():
            assert isinstance(k, str)
            if k == "people":
                people = v
            elif k == "milestones":
                milestones = v
            elif k == "bugzilla_url":
                if not isinstance(v, str):
                    raise ConfigParseError("`bugzilla_url` must be a string")
                bugzilla_url = v
            else:
                raise ConfigParseError(f"unknown config entry: `{k}`")

        if bugzilla_url is None:
            raise ConfigParseError("`bugzilla_url` field is missing")

        config = Config(bugzilla_url=bugzilla_url, people={}, milestones={})

        if people is None:
            raise ConfigParseError("`people` table is missing")
        else:
            config._parse_people(people)

        if milestones is None:
            raise ConfigParseError("`milestones` table is missing")
        else:
            config._parse_milestones(milestones)

        return config

    @staticmethod
    def from_str(text: str) -> "Config":
        try:
            parsed_toml = toml.loads(text)
        except toml.TomlDecodeError as e:
            new_err = ConfigParseError(f"TOML parse error: {e}")
            raise new_err.with_traceback(sys.exc_info()[2])
        return Config._from_toml(parsed_toml)

    @staticmethod
    def from_file(file: Any) -> "Config":
        if isinstance(file, list):
            raise TypeError("list is not a valid file or path")
        try:
            parsed_toml = toml.load(file)
        except toml.TomlDecodeError as e:
            new_err = ConfigParseError(f"TOML parse error: {e}")
            raise new_err.with_traceback(sys.exc_info()[2])
        return Config._from_toml(parsed_toml)
