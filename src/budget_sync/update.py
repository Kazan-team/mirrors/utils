import toml
from budget_sync.write_budget_csv import write_budget_csv
from bugzilla import Bugzilla
import logging
import argparse
from pathlib import Path
from budget_sync.config import Config, ConfigParseError, Milestone
from budget_sync.budget_graph import (BudgetGraph, BudgetGraphBaseError,
                                      PaymentSummary)
from budget_sync.write_budget_markdown import write_budget_markdown
from datetime import datetime, date

logging.basicConfig(level=logging.INFO)


def main():
    parser = argparse.ArgumentParser(
        description="Check for errors in "
        "Libre-SOC's style of budget tracking in Bugzilla.")
    parser.add_argument(
        "-c", "--config", type=argparse.FileType('r'),
        required=True, help="The path to the configuration TOML file",
        dest="config", metavar="<path/to/budget-sync-config.toml>")
    parser.add_argument(
        "-o", "--output-dir", type=Path, default=None,
        help="The path to the output directory, will be created if it "
        "doesn't exist",
        dest="output_dir", metavar="<path/to/output/dir>")
    parser.add_argument('--username', help="Log in with this username")
    parser.add_argument('--password', help="Log in with this password")
    parser.add_argument('--bug', help="bug number")
    parser.add_argument('--user', help="set payee user")
    parser.add_argument('--paid', help="set paid date")
    parser.add_argument('--submitted', help="set submitted date")
    args = parser.parse_args()
    try:
        with args.config as config_file:
            config = Config.from_file(config_file)
    except (IOError, ConfigParseError) as e:
        logging.error("Failed to parse config file: %s", e)
        return
    logging.info("Using Bugzilla instance at %s", config.bugzilla_url)
    bz = Bugzilla(config.bugzilla_url)
    if args.username:
        logging.debug("logging in...")
        bz.interactive_login(args.username, args.password)
    logging.debug("Connected to Bugzilla")
    bugs = str(args.bug).split(",")
    buglist = bz.getbugs(bugs)
    logging.info("got bugs %s" % args.bug)
    for bug in buglist:
        print("payees", bug.id)
        print("    "+"\n    ".join(bug.cf_payees_list.split('\n')))

        parsed_toml = toml.loads(bug.cf_payees_list)

        payee = parsed_toml[args.user]
        if isinstance(payee, int):
            payee = {'amount': payee}

        modified = False

        if args.submitted and 'submitted' not in payee:
            modified = True
            d = datetime.strptime(args.submitted, "%Y-%m-%d")
            payee['submitted'] = date(d.year, d.month, d.day)

        if args.paid and 'paid' not in payee:
            modified = True
            d = datetime.strptime(args.paid, "%Y-%m-%d")
            payee['paid'] = date(d.year, d.month, d.day)

        # skip over not modified
        if not modified:
            continue

        parsed_toml[args.user] = payee

        encoder = toml.encoder.TomlPreserveInlineDictEncoder()
        ttxt = toml.dumps(parsed_toml, encoder=encoder)
        print(ttxt)

        #update = bz.build_update(cf_payees_list=ttxt)
        bz.update_bugs([bug.id], {'cf_payees_list': ttxt})


if __name__ == "__main__":
    main()
