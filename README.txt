https://git.libre-soc.org/?p=utils.git;a=summary

clone with:
   git clone 'https://git.libre-soc.org/git/utils.git'

as root install with

    python3 setup.py develop

or as user, just remember to add ~/.local/bin to $PATH)

    python3 setup.py develop --user

run as:

    nohup budget-sync -c budget-sync-config.toml -o mdwn

to create JSON output with comment #0 for each MoU task:

    nohup budget-sync -c budget-sync-config.toml -o mdwn --comments

then examine the nohup.out and also the individual markdown files,
or compile to html first (after apt-get install python-markdown):

    markdown_py -f lkcl.html lkcl.mdwn

to perform multiple *DIRECT* database updates automatically, use the
following command:

    python3 ./src/budget_sync/update.py -c budget-sync-config.toml \
            --user {name_in_TOML_field} \
            --username {BUGZILLA_EMAIL} \
            --password '{BUGZILLA_PASSWORD}' \
            --bug NNN,MMM,OOO \
            --submitted=YYYY-MM-DD or --paid=YYYY-MM-DD

**PLEASE EXERCISE EXTREME CAUTION WHEN USING THIS COMMAND**

